WITH film_rentals AS (
    SELECT
        f.title,
        f.rating,
        COUNT(r.rental_id) AS rental_count
    FROM
        rental r
    INNER JOIN
        inventory i ON r.inventory_id = i.inventory_id
    INNER JOIN
        film f ON i.film_id = f.film_id
    GROUP BY
        f.film_id, f.title, f.rating
)
SELECT
    title,
    rating,
    rental_count
FROM
    film_rentals
ORDER BY
    rental_count DESC
LIMIT 5;