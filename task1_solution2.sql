WITH RevenueRank AS (
  SELECT s.store_id, st.staff_id, st.first_name, st.last_name,
         SUM(p.amount) AS total_revenue,
         RANK() OVER (PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) AS revenue_rank
  FROM payment p
  JOIN rental r ON p.rental_id = r.rental_id
  JOIN inventory i ON r.inventory_id = i.inventory_id
  JOIN store s ON i.store_id = s.store_id
  JOIN staff st ON p.staff_id = st.staff_id
  WHERE EXTRACT(YEAR FROM p.payment_date) = 2017
  GROUP BY s.store_id, st.staff_id
)
SELECT store_id, staff_id, first_name, last_name, total_revenue
FROM RevenueRank
WHERE revenue_rank = 1;
