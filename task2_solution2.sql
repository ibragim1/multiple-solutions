SELECT f.title, f.rating, COUNT(r.rental_id) AS rental_count,
       CASE
         WHEN f.rating = 'G' THEN 'All Ages'
         WHEN f.rating = 'PG' THEN 'Parental Guidance Suggested'
         WHEN f.rating = 'PG-13' THEN 'Teens and Up'
         WHEN f.rating = 'R' THEN 'Adults'
         ELSE 'General Audience'
       END AS expected_age
FROM rental r
JOIN inventory i ON r.inventory_id = i.inventory_id
JOIN film f ON i.film_id = f.film_id
GROUP BY f.film_id
ORDER BY rental_count DESC
LIMIT 5;
