WITH revenue AS (
    SELECT
        s.store_id,
        s.first_name || ' ' || s.last_name AS staff_name,
        SUM(p.amount) AS total_revenue,
        RANK() OVER (PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) as rank
    FROM
        staff s
    JOIN
        payment p ON s.staff_id = p.staff_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY
        s.store_id, s.staff_id, s.first_name, s.last_name
)
SELECT
    store_id,
    staff_name,
    total_revenue
FROM
    revenue
WHERE
    rank = 1
ORDER BY
    store_id;  

    